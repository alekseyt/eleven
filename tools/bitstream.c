#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <libeleven/l11.h>

#if defined(L11_32)
	typedef uint32_t l11_uint_t;
#	define l11_seed (l11_seed32)
#	define l11_rand (l11_rand32)
#	define L11_VARIANT "32"
#elif defined(L11_64)
	typedef uint64_t l11_uint_t;
#	define l11_seed (l11_seed64)
#	define l11_rand (l11_rand64)
#	define L11_VARIANT "64"
#else
#	error "Unsupported build option: need either L11_32 or L11_64"
#endif

void usage(const char *bin) {
	fprintf(stdout, "(%s) Usage: %s <FILENAME> <LENGTH>\n", L11_VARIANT, bin);
	fprintf(stdout, "\n");
	fprintf(stdout, "FILENAME - output filename (e.g. \"data.11\")\n");
	fprintf(stdout, "LENGTH   - output stream in bytes (multiple of %u, e.g. 18650752)\n", (unsigned)(sizeof(l11_uint_t)));
}

static void _byte_to_binary(char byte, char binary[8]) {
    int i = 0; int z = 0x80; for (; z > 0; z >>= 1, ++i) {
        binary[i] = ((byte & z) == z) ? '1' : '0';
    }
}

static int _write_buffer(const char *buffer, size_t len, const char *filename) {
	FILE *f = fopen(filename, "w+");
	if (f == 0) {
		int saved_errno = errno;
		fprintf(stderr, "Error opening file '%s': %s (%d)", filename, strerror(errno), errno);
		return -saved_errno;
	}

	fprintf(stdout, "Writing to '%s'... ", filename);

	{ size_t i = 0; for (; i < len; ++i) {
		char binary[8] = { 0 };

		_byte_to_binary(buffer[i], binary);

		if (fwrite(binary, sizeof(binary), 1, f) < 1) {
			int saved_errno = errno;
			fprintf(stderr, "Error writing to file '%s': %s (%d)\n", filename, strerror(errno), errno);
			return -saved_errno;
		}

		if ((i + 1) % 4 == 0) {
			fwrite("\n", 1, 1, f);
		}
	}}

	fprintf(stdout, "%u bytes\n", (unsigned)(len));

	fclose(f);
	return 0;
}

int main(int argc, char **argv) {
	if (argc < 3) {
		usage(argv[0]);
		exit(1);
	}

	const int buffer_len = atoi(argv[2]);
	if (buffer_len < 0) {
		usage(argv[0]);
		exit(1);
	}

	if (buffer_len == 0) {
		exit(0);
	}

	if (buffer_len % sizeof(l11_uint_t) != 0) {
		fprintf(stderr, "Due to this tool limitations, size of the buffer is need to be multiple of %u (l11_uint_t size)\n", (unsigned)(sizeof(int)));
		exit(1);
	}

	fprintf(stdout, "Generating bitstream...\n");

	int seed = l11_seed(time(0), 0);

	char *buffer = malloc(buffer_len);
	{ size_t i = 0; for (; i < buffer_len / sizeof(int); ++i) {
		*(((int *)buffer) + i) = l11_rand(l11_seed(i, seed));
	}}

	const char *filename = argv[1];
	int write_ret = _write_buffer(buffer, buffer_len, filename);

	free(buffer);

	return write_ret;
}
