#ifndef L11_TOOLS_CONV11_H
#define L11_TOOLS_CONV11_H

/* This is only a reference implementation for TestU01,
 * eleven was never intended to be used this way (with having
 * internal state) */

double eleven32();
double eleven64();

#endif /* L11_TOOLS_CONV11_H */
