#include <bbattery.h>
#include <unif01.h>

#include "conv11.h"

int main (void) {
	{ 
		unif01_Gen *gen = unif01_CreateExternGen01("eleven32", eleven32);
		bbattery_pseudoDIEHARD(gen);
		unif01_DeleteExternGen01(gen);
	}
	{
		unif01_Gen *gen = unif01_CreateExternGen01("eleven64", eleven64);
		bbattery_pseudoDIEHARD(gen);
		unif01_DeleteExternGen01(gen);
	}

	return 0;
}
