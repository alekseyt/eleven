#include <assert.h>
#include <time.h>
#include <libeleven/l11.h>

double eleven32() {
	static uint32_t seed = 0;
	static size_t offset = 0;

	if (seed == 0) {
		seed = l11_seed32(time(0), 0);
	}

	assert(seed != 0);

	return l11_randr32(l11_seed32(offset++, seed));
}

double eleven64() {
	static uint32_t seed = 0;
	static size_t offset = 0;

	if (seed == 0) {
		seed = l11_seed64(time(0), 0);
	}

	assert(seed != 0);

	return l11_randr64(l11_seed64(offset++, seed));
}
