[TOC]

## What is this

This is non crypto secure PRNG which can go back and forth in sequence
of random numbers (reversible PRNG[6]) at contant time. eleven exploits hash
function's avalanche effect to transform sequential numbers into random
numbers. Basically random number in eleven is a function of time, like in true
RNG, which accumulates entropy over time, but eleven can go backwards.

It uses Murmur2[1] hash function internally and this implementation is able to
pass most tests from strong statistical test suites (see below). Initially it
was using FNV-1a hash internally, however Murmur2 shown consistenly better
results. FNV variant could be found in Git revision history.

This was an experiment to make reversible RNG which can be reverted any
number of iterations backwards faster than with linear complexity. It provides
pool of random numbers which can be shared across distributed system without a
need to exchange additional traffic. If you have time mark of random event at
remote node, you can always figure out what random number (or sequence of
random numbers) that node rolled from timestamp of event.

This makes eleven predictable RNG, but it still produce pseudo random
numbers. The difference with non-reversible PRNG is that, knowing seed,
any number can be predicted at constant time instead of rolling numbers
one by one.

## Statistical testing

### NIST Statistical Test Suite

To run statistical tests it is required to build ``tools/bitstream`` to
generate data file with random bits and NIST STS[2][3] at your disposal to
run tests on this file.

    cmake .. -DL11_BUILD_SAMPLES=1
    make
    ./tools/bitstream data.11 18650752

This will create ``data.11`` file in current directory. Move it or link to 
NIST's STS data directory, or change testing script (see below).

    ./assess 100000 <script
    
Will run all test from NIST STS on ``data/data.11``. Testing script:

    0
    data/data.11
    1
    0
    100
    0

Test details:

* Generator selection: Input File
* Input file: ``data/data.11``
* Statistical tests: All
* Parameter adjustments: None
* Bitstreams: 100
* Input file format: ASCII

Results of the test normally located under ``experiments/AlgorithmTesting``
of NIST STS: ``finalAnalysisReport.txt`` etc.

    ------------------------------------------------------------------------------
    RESULTS FOR THE UNIFORMITY OF P-VALUES AND THE PROPORTION OF PASSING SEQUENCES
    ------------------------------------------------------------------------------
       generator is <data/data.11>
    ------------------------------------------------------------------------------
     C1  C2  C3  C4  C5  C6  C7  C8  C9 C10  P-VALUE  PROPORTION  STATISTICAL TEST
    ------------------------------------------------------------------------------
      9  14   5   5   7   9  12  11  17  11  0.153763     97/100     Frequency
      7  11  11  16   8  13   8  12   9   5  0.401199    100/100     BlockFrequency
     11   9  13   5   7   6  12  13  12  12  0.514124     98/100     CumulativeSums
     10   6   9   8  10   4  14  12  13  14  0.334538    100/100     CumulativeSums
     11  12  11  14   7   8   7   8  12  10  0.816537    100/100     Runs
      7   7  10  16  13   9   9   9   9  11  0.657933    100/100     LongestRun
     10   7  11  11   8   9  15  16   7   6  0.334538     98/100     Rank
     12   8  15   8  11   9  10  11   9   7  0.834308     97/100     FFT
    
    ...
    
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    The minimum pass rate for each statistical test with the exception of the
    random excursion (variant) test is approximately = 96 for a
    sample size = 100 binary sequences.

### TestU01

Requres TestU01[4][5] installed. Configure eleven with samples build enabled:

    cmake .. -DL11_BUILD_SAMPLES=1
    make
    ./tools/bitstream data.11 18650752

It should show lines similar to the following:

    -- Found TestU01 header file in /usr/local/include
    -- Found TestU01 libraries: /usr/local/lib/libtestu01.so

Meaning that TestU01 installation is found and will be used during build.
``make`` will build ``tools/smallcrush``, ``tools/crush`` and
``tools/bigcrush`` which would run TestU01's SmallCrush[5], Crush and BigCrush
test suites accordingly.

Note that it takes quite some time to run TestU01 suites: SmallCrush -
minutes, Crush and BigCrush - hours.

Results are printed to screen at the end of suite excution.

    ========= Summary results of BigCrush =========
    
     Version:          TestU01 1.2.3
     Generator:        eleven64
     Number of statistics:  160
     Total CPU time:   09:36:36.38
    
     All tests were passed

Note that there is evidence that eleven could fail ocassional tests in Crush,
thus it's reasonable to assume that it might also fail a test in BigCrush.

eleven also incorporates TestU01 based pseudo-DIEHARD test suite
``tools/pdiehard`` for informational purposes.

    ========= Summary results of pseudoDIEHARD =========
     
     Generator:        eleven64
     Number of statistics:  126
     Total CPU time:   00:00:28.14
     
     All tests were passed

## Usage examples

See ``samples/`` directory, built by passing ``-DL11_BUILD_SAMPLES=1`` to
CMake configuration:

    cmake .. -DL11_BUILD_SAMPLES=1

* Basic sequence of numbers: [sequence.c][]
* Decimal random numbers: [decimal.c][]
* Pulling past numbers: [backward.c][]

[sequence.c]: https://bitbucket.org/alekseyt/eleven/src/master/samples/sequence.c
[decimal.c]: https://bitbucket.org/alekseyt/eleven/src/master/samples/decimal.c
[backward.c]: https://bitbucket.org/alekseyt/eleven/src/master/samples/backward.c

For demonstrational purposes:

* Estimating value of π (Pi) using pairs of random integers: [pi11.c][]

[pi11.c]: https://bitbucket.org/alekseyt/eleven/src/master/samples/pi11.c

## Documentation

Maintained in Doxygen format in header files. Running

    doxygen

Will produce ``doc/html`` folder with documentation in browsable HTML.

## Tests and coverage

    cmake .. -DCMAKE_BUILD_TYPE=GCOV
    make coverage

Will run tests and produce coverage report (HTML) in ``tests/coverage``. Tests
are built by passing ``-DL11_BUILD_TESTS=11`` to CMake and could be executed
by running ``tests/units``. See also [BUILD][], see also ``tools/``
for statistical tests.

[BUILD]: 

## References

1. [Murmur2 hash](https://sites.google.com/site/murmurhash/)
2. [NIST Statistical Test Suite](http://csrc.nist.gov/groups/ST/toolkit/rng/documentation_software.html)
3. [A Statistical Test Suite for the Validation of Random Number Generators and Pseudo Random Number Generators for Cryptographic Applications](http://csrc.nist.gov/groups/ST/toolkit/rng/documents/SP800-22rev1a.pdf) (PDF)
5. [TestU01](http://simul.iro.umontreal.ca/testu01/tu01.html)
6. [A Software Library in ANSI C for Empirical Testing of Random Number Generators](http://www.iro.umontreal.ca/~simardr/testu01/guideshorttestu01.pdf) (PDF)
7. [Efficient Optimistic Parallel Simulations using Reverse Computation](http://www.cc.gatech.edu/computing/pads/PAPERS/rc-pads99.pdf) (PDF)

## Questions?

[aleksey.tulinov@gmail.com](mailto:aleksey.tulinov@gmail.com)
