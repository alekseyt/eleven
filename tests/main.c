#include <stdio.h>

typedef void (*testcase)(void);

void test_rng_seed();
void test_rng_rand();
void test_rng_randr();
void test_rng_rollback();

testcase cases[] = {
		test_rng_seed,
		test_rng_rand,
		test_rng_randr,
		test_rng_rollback,
};

int main() {
	unsigned i = 0; for (; i < sizeof(cases) / sizeof(*cases); ++i) {
		cases[i]();
		printf(".");
	}

	printf("\nOK - %u tests\n", i);
	return 0;
}
