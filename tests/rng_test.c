#include <assert.h>
#include <time.h>
#include <libeleven/fnv.h>
#include <libeleven/murmur2.h>
#include <libeleven/rng.h>

void test_rng_seed() {
	assert(l11_seed32(23, 11) == l11_murmur32(23, 11));
	assert(l11_seed64(23, 11) == l11_murmur64(23, 11));
}

void test_rng_rand() {
	int seed = time(0);
	assert(l11_rand32(seed) == l11_murmur32(seed, L11_FNV_OFFSET_BASIS32));
	assert(l11_rand64(seed) == l11_murmur64(seed, L11_FNV_OFFSET_BASIS64));
}

void test_rng_randr() {
	int seed = time(0);
	assert(l11_randr32(seed) ==
		l11_murmur32(seed, L11_FNV_OFFSET_BASIS32) / (double)(L11_RAND_MAX32));
	assert(l11_randr64(seed) ==
		l11_murmur64(seed, L11_FNV_OFFSET_BASIS64) / (double)(L11_RAND_MAX64));
}

void test_rng_rollback() { /* 64 bit variant */
	uint64_t numbers[5] = { 0 };
	const int len = sizeof(numbers) / sizeof(*numbers);

	int seed = l11_seed64(time(0), 0);
	int i = 0; for (; i < len; ++i) {
		numbers[i] = l11_rand64(l11_seed64(i, seed));
		assert(numbers[i] != 0);
	}

	i = len - 1; for (; i >= 0; --i) {
		assert(l11_rand64(l11_seed64(i, seed)) == numbers[i]);
	}
}
