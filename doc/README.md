Please refer to "Modules/API" section at the left for API Index.

* Project site: https://bitbucket.org/alekseyt/eleven
* Git: https://alekseyt@bitbucket.org/alekseyt/eleven.git

### Testing and coverage

    mkdir build && cd build
    cmake .. -DCMAKE_BUILD_TYPE=GCOV
    make coverage

* Test suite: tests/units
* Coverage report: tests/coverage/libeleven/index.html


### Release build
   
    mkdir build && cd build
    cmake .. -DCMAKE_BUILD_TYPE=Release
    make

### Statistical testing

* [https://bitbucket.org/alekseyt/eleven#markdown-header-statistical-testing](https://bitbucket.org/alekseyt/eleven#markdown-header-statistical-testing)

