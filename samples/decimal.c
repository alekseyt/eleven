#include <stdio.h>
#include <time.h>
#include <libeleven/l11.h>

/* This sample will produce a sequence of (pseudo) random numbers
 * in a range [0.0, 1.1]. For general details on sequential production
 * of rn please see sequence.c */

static const size_t SEQUENCE_LEN = 10;

int main() {
	uint64_t seed = l11_seed64(time(0), 0);

	size_t i = 0; for (; i < SEQUENCE_LEN; ++i) {
		printf("%.6lf\n", l11_randr64(l11_seed64(i, seed)));
	}
}
