#include <math.h>
#include <stdio.h>
#include <time.h>
#include <libeleven/l11.h>

/* Example of estimating Pi value using sequence
 * of generated random integers. It generates a sequence of random
 * integer pairs, establishes number of coprime integers and then
 * estimates Pi based on that number. Takes some time to establish
 * common denominator for all pairs of random integers, estimated
 * value of Pi is also not very accurate (should be more or less alright
 * for two decimal places as in 3.14). */

static const size_t SEQUENCE_LEN = 1000000000; /* Will be doubled for rand integers because this example gen pairs of numbers */
static const size_t PI11_RAND_MAX = 1000000000; /* Upper limit on random numbers */

/* Greatest common denominator */
uint64_t gcd(uint64_t x, uint64_t y) {
	while (y != 0) {
		const uint64_t temp = x % y;
		x = y;
		y = temp;
	}

	return x;
}

/* Test if two numbers share a factor */
int cofactor(uint64_t x, uint64_t y) {
	return (gcd(x, y) == 1 ? 0 : 1);
}

int main() {
	uint64_t seed = l11_seed64(time(0), 0);

	setbuf(stdout, 0); /* Disable buffering on stdout */
	printf("Working: ");

	size_t cofactor_count = 0;
	size_t i = 0; for (; i < SEQUENCE_LEN * 2; i += 2) {
		uint64_t x = l11_rand64(l11_seed64(i, seed)) % (PI11_RAND_MAX);
		uint64_t y = l11_rand64(l11_seed64(i + 1, seed)) % (PI11_RAND_MAX);

		if (cofactor(x, y) != 0) {
			++cofactor_count;
		}

		if (i % ((SEQUENCE_LEN * 2) / 10) == 0) {
			printf("%.0f%%..", (double)(i) / (SEQUENCE_LEN * 2) * 100);
		}
	}

	printf("\n");

	const size_t coprime_count = (SEQUENCE_LEN - cofactor_count);
	printf("Sequence: %lu (pairs), cofactor: %lu, coprime: %lu\n",
		SEQUENCE_LEN, cofactor_count, coprime_count);

	/* coprime prob: 6/(pi^2), pi=sqrt(6/(coprime prob)) */
	const double x = ((double)(coprime_count) / SEQUENCE_LEN);
	const double pi11 = sqrt(6.0 / x);

	printf("Pi (estimate): %lf\n", pi11);

	return 0;
}
