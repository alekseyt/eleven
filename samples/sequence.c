#include <stdio.h>
#include <time.h>
#include <libeleven/l11.h>

/* This sample will produce a sequence of (pseudo) random numbers.
 * The same method which is used in tools/bitstream.c for producing
 * bit stream for test against statistical test suite */

static const size_t SEQUENCE_LEN = 10;

int main() {
	/* create seed from current time */
	uint64_t seed = l11_seed64(time(0), 0);

	size_t i = 0; for (; i < SEQUENCE_LEN; ++i) {
		/* seed (the seed) is "offset" by mixing original
		seed value (a seed) with the value of i into new seed */
		uint64_t new_seed = l11_seed64(i, seed);
		printf("%u\n", (unsigned)(l11_rand64(new_seed)) % 100);
	}
}
