#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <libeleven/l11.h>

/* This example visually demostrates ability of eleven to go
 * into backward direction. It will print sequence or random numbers
 * alongside with the same sequence shifted in the past (visually
 * future, it's hard to explain). Computation is done in contant time. */

static const size_t SEQUENCE_LEN = 10;

int main() {
	uint64_t seed = l11_seed64(time(0), 0);
	size_t i = 0; for (; i < SEQUENCE_LEN; ++i) {
		/* natural order */
                printf("%2u ", (unsigned)(l11_rand64(l11_seed64(i, seed)) % 100));
		if (i >= SEQUENCE_LEN / 2) {
			/* half sequence bacwards */
			uint64_t new_seed = l11_seed64(i - SEQUENCE_LEN / 2, seed);
                	printf("%2u", (unsigned)(l11_rand64(new_seed) % 100));
		}
		printf("\n");
	}
}
