include_directories(${CMAKE_SOURCE_DIR})
link_libraries(eleven)

add_executable(sequence sequence.c)
add_executable(decimal decimal.c)
add_executable(backward backward.c)

link_libraries(m)
add_executable(pi11 pi11.c)
