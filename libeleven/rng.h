#ifndef L11_RNG_H
#define L11_RNG_H

#include <stdint.h>

/** @file
 *
 * PRNG functions.
 */

/** @defgroup api API
 *
 * There are basically two functions: 1) for seeding 2) for retrieving a number.
 * However unlike conventional PRNG which is seeded once and then used to
 * produce multiple random numbers, eleven could be seeded multiple times, but
 * final seed produce single random number. Seeding is only a way to randomize
 * sequence of numbers produced.
 *
 * Functions provided in two flawors: 32 and 64 bits. 64 variant is recommeneded
 * even on 32 bit systems.
 *
 * @example backward.c
 * @example decimal.c
 * @example sequence.c
 *
 * @defgroup other Other
 *
 * Functions for internal use.
 */

#if defined (__cplusplus) || defined (c_plusplus)
extern "C" {
#endif

#include "config.h"

/**
 * @ingroup api
 * @see l11_seed64
 */
uint32_t l11_seed32(uint32_t theseed, uint32_t aseed);

/**
 * @ingroup api
 * @see l11_rand64
 */
uint32_t l11_rand32(uint32_t seed);

/**
 * @ingroup api
 * @see l11_randf64
 */
double l11_randr32(uint32_t seed);

/** Produce seed from a number, optionally from multiple numbers.
 *
 * Seed is produced by applying hash function to the seed values.
 * Optionally, if "a seed" is passed as well, it will be used as basis
 * for producing new seed by applying the same hash function.
 *
 * @code
 * seed = l11_seed(time(0), 0);
 * sleep(1);
 * seed = l11_seed(time(0), seed)); // accumulate time in seed
 * @endcode
 *
 * @ingroup api
 * @param theseed seed number
 * @param aseed previous seed value or 0 if none
 * @return seed value
 */
uint64_t l11_seed64(uint64_t theseed, uint64_t aseed);

/** Get number from seed.
 *
 * It will always return the same number for the same seed,
 * if you need to "offset" returned number - you might
 * include offset into seed:
 *
 * @code
 * l11_rand(l11_seed(10, seed)); // 10 is the "offset"
 * @endcode
 *
 * @see l11_seed
 *
 * @ingroup api
 * @param seed ideally product of l11_seed(), @see l11_seed64
 * @return uniformly distributed number
 */
uint64_t l11_rand64(uint64_t seed);

/** Get decimal number from seed in range [0.0, 1.0]
 *
 * Has same constraints as l11_rand(). @see l11_rand64
 *
 * @ingroup api
 * @param seed ideally product of l11_seed()
 * @return number in range [0.0, 1.0]
 */
double l11_randr64(uint64_t seed);

#if defined (__cplusplus) || defined (c_plusplus)
}
#endif

#endif /* L11_RNG_H */
