#include "murmur2.h"

const uint32_t L11_MURMUR_MU32 = 0x5bd1e995;
const uint32_t L11_MURMUR_R32  = 24;
const uint64_t L11_MURMUR_MU64 = 0xc6a4a7935bd1e995L;
const uint64_t L11_MURMUR_R64  = 47;
