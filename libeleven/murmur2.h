#ifndef L11_MURMUR2_H
#define L11_MURMUR2_H

#include <stddef.h>
#include <stdint.h>

/** @file */

#if defined (__cplusplus) || defined (c_plusplus)
extern "C" {
#endif

/** MU (multiply) value for MurmurHash2 (32 bits variant) */
extern const uint32_t L11_MURMUR_MU32;
/** R (rotate) value for MurmurHash2 (32 bits variant) */
extern const uint32_t L11_MURMUR_R32;
/** MU (multiply) value for MurmurHash2 (64 bits variant) */
extern const uint64_t L11_MURMUR_MU64;
/** R (rotate) value for MurmurHash2 (64 bits variant) */
extern const uint64_t L11_MURMUR_R64;

/** Compute Murmur2 hash (32 bits variant). For internal use.
 *
 * @ingroup other
 * @param x value to hash
 * @param seed return value of l11_seed32
 * @return hash value
 */
static inline
uint32_t l11_murmur32(uint32_t x, uint32_t seed) {
	uint32_t hash = seed;

	x *= L11_MURMUR_MU32;
	x ^= x >> L11_MURMUR_R32;
	x *= L11_MURMUR_MU32;

	hash *= L11_MURMUR_MU32;
	hash ^= x;

	hash ^= hash >> 13;
	hash *= L11_MURMUR_MU32;
	hash ^= hash >> 15;

	return hash;
}

/** Compute Murmur2 hash (64 bits variant). For internal use.
 *
 * @ingroup other
 * @param x value to hash
 * @param seed return value of l11_seed64
 * @return hash value
 */
static inline
uint64_t l11_murmur64(uint64_t x, uint64_t seed) {
	uint64_t hash = seed;

	x *= L11_MURMUR_MU64;
	x ^= x >> L11_MURMUR_R64;
	x *= L11_MURMUR_MU64;

	hash *= L11_MURMUR_MU64;
	hash ^= x;

	hash ^= hash >> L11_MURMUR_R64;
	hash *= L11_MURMUR_MU64;
	hash ^= hash >> L11_MURMUR_R64;

	return hash;
}

#if defined (__cplusplus) || defined (c_plusplus)
}
#endif

#endif /* L11_MURMUR2_H */
