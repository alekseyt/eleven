#ifndef L11_FNV_INTERNAL_H
#define L11_FNV_INTERNAL_H

#include <stddef.h>
#include <stdint.h>

/** @file */

#if defined (__cplusplus) || defined (c_plusplus)
extern "C" {
#endif

/** FNV offset basis for FNV-32a */
extern const uint32_t L11_FNV_OFFSET_BASIS32;
/** FNV offset basis for FNV-64a */
extern const uint64_t L11_FNV_OFFSET_BASIS64;

#if defined (__cplusplus) || defined (c_plusplus)
}
#endif

#endif /* L11_FNV_INTERNAL_H */
