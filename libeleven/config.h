#ifndef L11_CONFIG_H
#define L11_CONFIG_H

/** @file */

/** Maximum number possibly returned from l11_rand(),
 * which is all-bits-set word.
 *
 * @see l11_rand
 * @see l11_randf
 */
#define L11_RAND_MAX32 ((uint32_t)(-1))

/**
 * @see L11_RAND_MAX32
 */
#define L11_RAND_MAX64 ((uint64_t)(-1))

#endif /* L11_CONFIG_H */
