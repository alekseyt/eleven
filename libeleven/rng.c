#include <assert.h>

#include "config.h"
#include "fnv.h"
#include "rng.h"
#include "murmur2.h"

uint32_t l11_seed32(uint32_t theseed, uint32_t aseed) {
	return l11_murmur32(theseed, aseed);
}

uint32_t l11_rand32(uint32_t seed) {
	return l11_murmur32(seed, L11_FNV_OFFSET_BASIS32);
}

double l11_randr32(uint32_t seed) {
	return l11_rand32(seed) / (double)(L11_RAND_MAX32);
}

uint64_t l11_seed64(uint64_t theseed, uint64_t aseed) {
	return l11_murmur64(theseed, aseed);
}

uint64_t l11_rand64(uint64_t seed) {
	return l11_murmur64(seed, L11_FNV_OFFSET_BASIS64);
}

double l11_randr64(uint64_t seed) {
	return l11_rand64(seed) / (double)(L11_RAND_MAX64);
}
